<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Declaration extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'declaration',
    ];

    public function user ()
    {
        $this->belongsTo(User::class);
    }

}
