// import * as VeeValidate from 'vee-validate';

require('./bootstrap');
window.Vue = require('vue');



// Vue.use(VeeValidate);


const app = new Vue({
    el: '#app',
    components:{
        HomeSite: () => import('./components/website/Home'),
        LoginSite: () => import('./components/website/Login'),

        HomeIndex: () => import('./components/app/home/HomeIndex'),
    }

});
